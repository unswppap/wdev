## !! IMPORTANT !!
This is the full version of SLCSE, it could find similar documents more precise, but **very storage and RAM consuming**.

Codes of the website is complete, but **additional files are required** to run this website:

1. Download NSWSC files from https://www.dropbox.com/s/xkx9y2kso8qun97/

2. Extract all files to somewhere, and set this path in config.py

3. Download compressed data file lshf.zip, extract and move it to ./data (decompressed file would be more than 40G)

4. Run python3 app.py
