import numpy as np
import heapq
import nltk
import string
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem.lancaster import LancasterStemmer
import re
import json
import operator
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer

'''
This file is used to Similarity between key words and documents,
sentences and documents or document and documentswith using
the TFIDF algorithm and return the similarity after comparing.
'''


# This method is used to preprocess the input data
def get_real_words(article):
    sens = nltk.sent_tokenize(article)

    all_sentences_in_words = [nltk.word_tokenize(sentence) for sentence in sens]

    words = []
    for sen_words in all_sentences_in_words:
        for word in sen_words:
            if word.isupper() and len(word) > 1:
                words.append(word)
            elif word.lower() not in stop_words and \
                    len(word) > 1 and \
                    word.lower() not in string.punctuation and \
                    word.lower().isalpha():
                words.append(word.lower())

    tags = nltk.pos_tag(words)

    real_words = []

    lem = WordNetLemmatizer()
    for i in range(len(words)):
        tag = tags[i]
        if re.match('V', tag[1]):
            tmp = lem.lemmatize(words[i], pos='v')
        else:
            tmp = lem.lemmatize(words[i])
        st_word = st.stem(tmp)
        real_words.append(st_word)
    return real_words


# Adding ' ' between words
def transform_real_words(real_words):
    transformed_words = []
    string = ' '.join(real_words)
    transformed_words.append(string)
    return transformed_words


# Calculate the value of TFIDF for each words
def calculate_tfidf(words):
    corpus = words
    vectorizer = CountVectorizer()
    transformer = TfidfTransformer()
    tfidf = transformer.fit_transform(vectorizer.fit_transform(corpus))
    word = vectorizer.get_feature_names()
    weight = tfidf.toarray()
    tfidf = {}
    sorted_tfidf = {}
    for i in range(len(weight)):
        for j in range(len(word)):
            tfidf[word[j]] = weight[i][j]
    temp = sorted(tfidf.items(), key=operator.itemgetter(1), reverse=True)
    for pairwise in temp:
        sorted_tfidf[pairwise[0]] = float("%.8f" % pairwise[1])
    return sorted_tfidf


# Process the whole input and get the TFIDF
def process_input(input_data):
    real_words = get_real_words(input_data)
    words = transform_real_words(real_words)
    tfidf = calculate_tfidf(words)
    return tfidf


# Calculate the cosine similarity
def cosine_dis(x, y):
    num = sum(map(float, x * y))
    denom = np.linalg.norm(x) * np.linalg.norm(y)
    if denom == 0:
        return 0
    else:
        return round(num / float(denom), 4)


# Sort the dictionary by value
def sort_by_value(dic):
    s_dic = sorted(dic.items(), key=lambda d: d[1], reverse=True)
    return s_dic


# Get the top k (key:value) of the dictionary
def dic_topn(dic, topN):
    compared_dic = {}
    topNum = topN
    if len(dic) > topN:
        nlargestList = heapq.nlargest(topNum, dic.values())
        for value in nlargestList:
            for key in dic:
                if dic[key] == value:
                    compared_dic[key] = value
    else:
        return dic
    return compared_dic


# According to input words or key words to find related documents
def find_documents(compared_dic, dic, number_of_find_doc):
    similarity_index = {}
    for key in compared_dic:
        if key in dic.keys():
            document_list = dic[key]
            if len(document_list) > number_of_find_doc:
                i = 0
                while i < number_of_find_doc:
                    similarity_index[document_list[i][0]] = 0
                    i = i + 1
            else:
                j = 0
                while j < len(document_list):
                    similarity_index[document_list[j][0]] = 0
                    j = j + 1
        else:
            continue
    return similarity_index


# Form the vector for calculating similarity
def form_vector_similarity(b, d, e, Top_n_doc):
    similarity = {}
    for key in e:
        input_L = []
        document_L = []
        temp = {}
        weight_each_doc = dic_topn(b[str(key)], Top_n_doc)
        for key1 in d:
            temp[key1] = '0'
        for key2 in weight_each_doc:
            temp[key2] = '0'
        for k in temp:
            if k in d and k in weight_each_doc:
                input_L.append(d[k])
                document_L.append(weight_each_doc[k])
            elif k not in d and k in weight_each_doc:
                input_L.append(0)
                document_L.append(weight_each_doc[k])
            elif k in d and k not in weight_each_doc:
                input_L.append(d[k])
                document_L.append(0)
            else:
                input_L.append(0)
                document_L.append(0)
        sim = cosine_dis(np.array(input_L), np.array(document_L))
        similarity[key] = sim
    return similarity


# Calculate the similarity
def calculate_similarity(inputdata, symbol, category, one_article_tfidf, number_set):
    similarity = {}
    input_tfidf = dic_topn(process_input(inputdata), number_set)
    for i in category[symbol]:
        temp = {}
        input_L = []
        document_L = []
        doc_tfidf = dic_topn(one_article_tfidf[str(i)], number_set)
        for key1 in input_tfidf:
            temp[key1] = '0'
        for key2 in doc_tfidf:
            temp[key2] = '0'
        for k in temp:
            if k in input_tfidf and k in doc_tfidf:
                input_L.append(input_tfidf[k])
                document_L.append(doc_tfidf[k])
            elif k not in input_tfidf and k in doc_tfidf:
                input_L.append(0)
                document_L.append(doc_tfidf[k])
            elif k in input_tfidf and k not in doc_tfidf:
                input_L.append(input_tfidf[k])
                document_L.append(0)
            else:
                input_L.append(0)
                document_L.append(0)
            sim = cosine_dis(np.array(input_L), np.array(document_L))
            similarity[i] = sim
    result = sort_by_value(dic_topn(similarity, 100))
    return result


# Get the results of similarity between documents
def get_final_similarity(inputs, one_article_tifidf, inverted_index, article_index, number_of_find_doc, Top_n_input,
                         Top_n_doc, category, symbol='all'):
    if symbol == 'all':
        input_tfidf_dic = process_input(inputs)
        top_k_words_of_inputdata = dic_topn(input_tfidf_dic, Top_n_input)
        related_document = find_documents(top_k_words_of_inputdata, inverted_index, number_of_find_doc)
        similarity_dic = form_vector_similarity(one_article_tifidf, top_k_words_of_inputdata, related_document,
                                                Top_n_doc)
        result = sort_by_value(similarity_dic)
        return result
    else:
        result = calculate_similarity(inputs, symbol, category, one_article_tfidf, Top_n_doc)
        return result


# Initial the data and load the existing data
print('initializing keyword engine')
stop_words = set(stopwords.words('english'))
st = LancasterStemmer()
lem = WordNetLemmatizer()

with open('./data/one_article_tfidf.min.json', 'r', encoding='utf-8') as f:
    print('\tloading tf-idf')
    one_article_tfidf = json.load(f)

with open('./data/inverted_index.min.json', 'r', encoding='utf-8') as g:
    print('\tloading inverted index')
    inverted_index = json.load(g)

with open('./data/output_article_index.min.json', 'r', encoding='utf-8') as h:
    print('\tloading article index')
    article_index = json.load(h)

with open('./data/cluster.min.json', 'r', encoding='utf-8') as i:
    print('\tloading cluster')
    category = json.load(i)


# Get final results [[document index: similarity],......]
def get_by_words(search_for, cluster='all'):
    return get_final_similarity(search_for, one_article_tfidf, inverted_index, article_index, 20, 40, 40, category, str(cluster))
