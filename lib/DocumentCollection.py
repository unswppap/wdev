import json
import math
from lib.Document import Document


class DocumentCollection:
    """
    Used by Document, an empty collection of documents, or recover from cache
    """

    def __init__(self, sort_by=None, recovery=None):
        self.documents = []
        self.sort_by = 'relevance' if sort_by is None else sort_by
        if recovery:
            self.recovery(recovery)

    def add_document(self, document):
        self.documents.append(document)

    def __len__(self):
        return len(self.documents)

    def quantity(self):
        return len(self)

    def sort(self, by):
        """
        sort results by relevance or date
        :param by: sort by
        """
        assert by in ['relevance', 'date']
        if by == 'date':
            self.documents.sort(key=lambda x: x.date, reverse=True)
            self.sort_by = 'date'
        elif by == 'relevance':
            self.documents.sort(key=lambda x: x.relevance, reverse=True)
            self.sort_by = 'relevance'

    def between(self, date1, date2):
        """
        :param date1: from date
        :param date2: to date
        :return: a new collection of results in the region
        """
        assert date1 < date2
        result = DocumentCollection(sort_by=self.sort_by)
        for document in self.documents:
            if date1 < document.date < date2:
                result.add_document(document)
        return result

    def in_category(self, category):
        """
        :param category: filter by this category
        :return: a collection of filtered results
        """
        if category != 'all':
            category_lookup = {
                'accident': '0',
                'enterprise': '1',
                'family': '2',
                'government': '4',
                'drug': '6',
                'murder': '7',
                'social': '9',
                'other': None,
            }
            assert category in category_lookup
            result = DocumentCollection(sort_by=self.sort_by)
            with open('./data/cluster.min.json') as file:
                clusters = json.load(file)
            if category != 'other':
                cluster = set(clusters[category_lookup[category]])
            else:
                cluster = set(clusters['3'] + clusters['5'] + clusters['8'])
            for document in self.documents:
                if document.index in cluster:
                    result.add_document(document)
            return result
        else:
            return self

    def get_page(self, page, rpp=50):
        """
        :param page: which page is it
        :param rpp: how many Results Per Page
        :return: a list of documents
        """
        index1 = (page - 1) * rpp
        index2 = page * rpp
        return self.documents[index1: index2]

    def get_page_quantity(self, rpp=50):
        """
        :param rpp: how many Results Per Page
        :return: how many pages needed
        """
        return math.ceil(self.quantity() / rpp)

    def recovery(self, content):
        assert type(content) == dict
        self.documents = [Document(mode='recovery', recovery=document) for document in content['documents']]
        self.sort_by = content['sort_by']

    def serialize(self):
        serialized = {'documents': [], 'sort_by': self.sort_by}
        for document in self.documents:
            serialized['documents'].append(document.serialize())
        return serialized
