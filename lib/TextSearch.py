"""Author: Xinjie LU  z5101488
this file used to get similiar articles
we first load the model, after runing this file, we can get a dictionary
{docID1: similarity1, docID2: similarity}
for client use, we only remain top 100 similiar articles
"""

import nltk
import string
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import re
from nltk.stem.lancaster import LancasterStemmer
import sklearn.externals.joblib as joblib

stop_words = set(stopwords.words('english'))
st = LancasterStemmer()
lem = WordNetLemmatizer()

# load the model
lshf = joblib.load('./data/lshf')
tfidf_vectorizer = joblib.load('./data/tfidf')


# deal with the input data, the same as preprocessing
def get_real_words(article):
    sens = nltk.sent_tokenize(article)
    all_sentences_in_words = [nltk.word_tokenize(sentence) for sentence in sens]
    words = []
    for sen_words in all_sentences_in_words:
        for word in sen_words:
            if word.isupper() and len(word) > 1:
                words.append(word)
            elif word.lower() not in stop_words and \
                    len(word) > 1 and \
                    word.lower() not in string.punctuation and \
                    word.lower().isalpha():
                words.append(word.lower())
    tags = nltk.pos_tag(words)
    real_words = []
    lem = WordNetLemmatizer()
    for i in range(len(words)):
        tag = tags[i]
        if re.match('V', tag[1]):
            tmp = lem.lemmatize(words[i], pos='v')
        else:
            tmp = lem.lemmatize(words[i])
        st_word = st.stem(tmp)
        real_words.append(st_word)
    return real_words


def transform_real_words(real_words):
    string = ' '.join(real_words)
    return string


def search(content):
    # input data
    test_cut_raw = get_real_words(content)
    test_cut_raw_1 = transform_real_words(test_cut_raw)
    x_test = tfidf_vectorizer.transform([test_cut_raw_1])

    # get the similarity articles
    distances, indices = lshf.kneighbors(x_test.toarray(), n_neighbors=100)

    # use dictionary to store the similarity information
    all_dict = {}
    for i in range(len(distances)):
        for j in range(len(distances[i])):
            all_dict[indices[i][j]] = 1 - distances[i][j]
    return all_dict
