from flask import url_for


class Pagination:
    """
    Generate pagination information by configs
    """
    def __init__(self, module, args, page_quantity, limit=20):
        """
        :param module: url to which module, i.e. url_for('index')
        :param args: arguments when generating urls
        :param page_quantity: how many pages in total
        :param limit: how many pages will be displayed at most
        """
        self.module = module
        self.args = args
        self.page_quantity = page_quantity
        self.limit = limit

    def set_limit(self, limit):
        """
        :param limit: set limit
        """
        self.limit = limit

    def get_pages(self, present):
        """
        Generate pagination information for one page
        :param present: present page number
        :return: a list of page information(in dictionary)
        """
        pages = []
        if present - int(self.limit / 2) < 1:
            start = 1
        else:
            start = present - int(self.limit / 2)
            self.args['page'] = 1
            pages.append({
                'url': url_for(self.module, **self.args),
                'style': '',
                'id': 1
            })
            pages.append({
                'url': '',
                'style': ' disabled',
                'id': '...'
            })
        if present + int(self.limit / 2) > self.page_quantity + 1:
            end = self.page_quantity + 1
        else:
            end = present + int(self.limit / 2)

        for i in range(start, end):
            self.args['page'] = i
            pages.append({
                'url': url_for(self.module, **self.args),
                'style': '' if present != i else ' active',
                'id': i
            })

        if end < self.page_quantity + 1:
            self.args['page'] = self.page_quantity
            pages.append({
                'url': '',
                'style': ' disabled',
                'id': '...'
            })
            pages.append({
                'url': url_for(self.module, **self.args),
                'style': '',
                'id': self.page_quantity
            })

        return pages
