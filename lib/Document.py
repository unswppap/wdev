from datetime import datetime
from flask import url_for
import re
import html
import config


class Document:
    def __init__(self, file_name=None, mode='view', index=None, search_for=None, relevance=None, recovery=None):
        assert mode in ['list', 'view', 'recovery']
        self.valid = True
        self.file_name = file_name
        self.index = int(index) if index is not None else None
        self.directory = None
        self.relevance = relevance
        self.relevance100 = f'{relevance * 100:.02f}%' if relevance is not None else None
        self.content_html = None
        self.title = None
        self.author = None
        self.date = None
        self.url = None
        self.url_plain = None
        self.excerpt = None

        if mode == 'list':
            assert search_for is not None
            self.directory = f'{config.nswsc_directory}/NSWSC_' + file_name + '.html'

            with open(self.directory) as file:
                file_content = file.read()
                self.content_html = re.findall('<body.*?>.*?<hr.*?>(.*)</body>', file_content.replace('\n', ''))[0]
                self.title = re.findall('<h2.*?>\s+(.*)\s+</h2>', file_content)[0]
                self.author = re.findall('<h1.*?>\s*(.*)\s*</h1>', file_content)[0]
                update = re.findall(
                    '(\d{0,2} ?(January|February|March|April|May|June|July|August|September|October|November|December) ?\d{4})',
                    file_content)[0][0]
                self.date = datetime.strptime(update, '%d %B %Y')

            self.url = url_for('view', document_name=file_name, high=search_for)
            self.url_plain = url_for('view', document_name=file_name)
            keywords = search_for.replace('\t', ' ').split(' ')
            content = html.unescape(re.sub("<.*?>", ' ', self.content_html))
            find_keyword = None
            for keyword in keywords:
                if keyword.lower() in content.lower():
                    find_keyword = keyword
                    break
            if find_keyword is not None:
                index = content.find(find_keyword)
                self.excerpt = content[index - 200 if index - 200 > 0 else 0: index + 200]
            else:
                print(f'{keywords} not found in {file_name}')
                self.valid = False
        elif mode == 'view':
            self.directory = f'{config.nswsc_directory}/NSWSC_' + file_name + '.html'
            with open(self.directory) as file:
                file_content = file.read()
                self.content_html = re.findall('<body.*?>.*?<hr.*?>(.*)</body>', file_content.replace('\n', ''))[0]
        else:
            self.recovery(recovery)

    def recovery(self, recovery):
        self.valid = recovery['valid']
        self.file_name = recovery['file_name']
        self.index = recovery['index']
        self.directory = recovery['directory']
        self.relevance = recovery['relevance']
        self.relevance100 = recovery['relevance100']
        self.content_html = recovery['content_html']
        self.title = recovery['title']
        self.author = recovery['author']
        self.date = datetime.strptime(recovery['date'], '%Y%m%d')
        self.url = recovery['url']
        self.url_plain = recovery['url_plain']
        self.excerpt = recovery['excerpt']

    def serialize(self):
        serialized = vars(self).copy()
        serialized['date'] = serialized['date'].strftime('%Y%m%d')
        return serialized
