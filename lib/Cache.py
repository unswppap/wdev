import hashlib
import os
import json


class Cache:
    def __init__(self, target=None, index=None):
        assert target or index
        self.target = None
        self.index = None
        if target:
            self.set_target(target)
        else:
            self.set_index(index)
        self.directory = f'./cache/{self.index}'
        self.exist = self.check_exist()
        self.content = None
        if self.exist:
            self.read()

    def set_target(self, target):
        self.target = target
        self.index = hashlib.md5(target.encode('utf-8')).hexdigest()

    def set_index(self, index):
        self.index = index

    def check_exist(self):
        self.exist = os.path.isfile(self.directory)
        return self.exist

    def read(self):
        with open(self.directory, 'r') as cache_file:
            self.content = json.load(cache_file)
        return self

    def store(self, content):
        self.content = content
        with open(self.directory, 'w') as cache_file:
            cache_file.write(json.dumps(self.content))

    def get(self):
        if self.exist:
            return self.content
        else:
            return None
